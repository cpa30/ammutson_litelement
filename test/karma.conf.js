const webpackTestConfig = require('./karma-webpack.config');

module.exports = function (config) {
    return config.set({
        browsers: ['Chrome'],
        reporters: ['progress', 'mocha'],
        webpack: webpackTestConfig,
        colors: true,
        frameworks: ['mocha'],
        basePath: process.cwd()+'/test/',
        files: [
            'all.tests.js'
        ],
        preprocessors: {
            'all.tests.js': ['webpack', 'coverage']
        },
        client: {
            clearContext: false,
            captureConsole: true
        },
        singleRun: false,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        port: 9876,
        concurrency: Infinity
    });
}