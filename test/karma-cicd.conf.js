const webpackTestConfig = require('./karma-webpack.config');

module.exports = function (config) {
    return config.set({
        browsers: ['HeadlessChrome'],
        reporters: ['progress', 'coverage'],
        customLaunchers: {
            HeadlessChrome: {
                base: "Chrome",
                flags: [
                    "--headless",
                    "--no-sandbox",
                    "--remote-debugging-port=9222"
                ]
            }
        },
        webpack: webpackTestConfig,
        colors: true,
        frameworks: ['mocha'],
        basePath: process.cwd()+'/test/',
        files: [
            'all.tests.js'
        ],
        preprocessors: {
            'all.tests.js': ['webpack', 'coverage']
        },
        singleRun: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        port: 9876,
        concurrency: Infinity
    });
}