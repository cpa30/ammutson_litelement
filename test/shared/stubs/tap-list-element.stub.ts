import { LitElement, css, property, customElement, html  } from 'lit-element';
import { Section } from '../../../src/app/shared/interfaces/Untappd';

@customElement('tap-list-element')
export class TapListElement extends LitElement {

    @property()
    name: string = 'Tap List Stub';

    @property({type: String, attribute: true})
    display: string;

    constructor() {
        super();
        super.connectedCallback();
    }

    connectedCallback() {
        this.fetchMenu();
    }

    async fetchMenu() {
    }

    static get styles() {
        const style = css`
            :host {
                background-color: rgba(0,0,0,1);
            }
            .title {
                font-family: Gobold Bold;
                text-align: center;
            }
        `;
        return [style];
    }

    public renderTable(tableName: string, tableContents: Section) {
        return html`
            <h1>Stub</h1>
        `;
    }

    render() {
        switch (this.display) {
            case 'tap':
                return this.renderTable(this.name, null);
            case 'fridge':
                return this.renderTable(this.name, null);
            default:
                return html`<span>Nothing here yet...</span>`;
        }
    }
}