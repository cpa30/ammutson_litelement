import { Options, Methods } from '../../../src/app/shared/interfaces/Http';

export class FakeAjaxService implements Methods {
    private _headers: Headers;

    constructor(headers?: Headers) {
        this._headers = headers;
    }

    public get headers() {
        return this._headers;
    }

    public set headers(value: Headers) {
        this._headers = value;
    }
    async get(url: string, options?: Options): Promise<Response> {
        return new Promise<Response>(() => {});
    }

    async post(url: string, json: string, options?: Options): Promise<Response> {
        return new Promise<Response>(() => {});
    }

    async put(url: string, json: string, options?: Options): Promise<Response> {
        return new Promise<Response>(() => {});
    }

    async delete(url: string, options?: Options): Promise<Response> {
        return new Promise<Response>(() => {});
    }
}