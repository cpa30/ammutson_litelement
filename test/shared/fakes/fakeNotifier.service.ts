import { Notifier } from "../../../src/app/shared/interfaces/notifier/notifier";

export class FakeNotifier implements Notifier {

    constructor() {}

    fetchMessagesForUser(userID: string): Promise<unknown> {
        return new Promise((resolve, reject) => {
            resolve({ data: [{id: "1234", text: "fake message"}], meta: {newest_id: '123535123', oldest_id: '123352123512', result_count: 1}});
            reject("Doesn't work");
        });
    }

    async fetchMessage(uRI: string): Promise<unknown> {
        return "fakeMessage";
    }
}