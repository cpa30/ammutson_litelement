// This is a webpack style config file for unit testing using mocha
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const ts_loader = {
    loader: 'ts-loader',
    options: {
        configFile: path.join(__dirname, '/tsconfig.json')
    }
}
 
module.exports = {
    target: "web",
    mode: 'development',
    plugins: [
        new CleanWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ts_loader,
                exclude: /node_modules/
            },
            { 
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            }
        ]
    },
    resolve: {
        extensions: ['.ts', '.js'],
        fallback: {
            "assert": false
        }
    },
    devtool: 'eval-source-map',
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    }
};