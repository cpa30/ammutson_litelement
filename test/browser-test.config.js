// This is a webpack style config file for unit testing using mocha
const HtmlWebpackPlugin = require('html-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const path = require('path');

const ts_loader = {
    loader: 'ts-loader',
    options: {
        configFile: path.join(__dirname, '/tsconfig.json')
    }
}
const webpackShellPluginCommand = new WebpackShellPlugin({
    onBuildExit: [
        'mocha ./build/tests.bundle.js',
    ]
});
 
// The configuration upon live Reload gives TS2306: <file> not a module error. Should investigate. For now karma test runner is used instead.
module.exports = ({ mode }) => {
    return {
        mode,
        entry: path.resolve(__dirname, 'all.tests.js'),
        output: {
            path: path.join(__dirname, '/build'),
            filename: 'tests.bundle.js',
        },
        target: "web",
        node: {
            fs: 'empty'
        },
        //externals: [nodeExternals()],
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, 'test.html')
            }),
            //webpackShellPluginCommand
        ],
        module: {
        rules: [
            {
                test: /\.ts$/,
                use: ts_loader,
                exclude: /node_modules/
            },
            {
                test: /\.test\.js$/,
                use: 'mocha-loader',
                exclude: /node_modules/
            },
            {
                test: /\.test\.ts$/,
                use: ['mocha-loader', ts_loader],
                exclude: /node_modules/
            },
            { 
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            }
            ]
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        devtool: mode === 'development' ? 'source-map' : 'none',
        watchOptions: {
            ignored: /node_modules/
        },
        devServer: {
            contentBase: path.join(__dirname, '/build'),
            compress: true,
            open: {
                app: ['chromium'],
                //app: ['chromium', '--headless', '--remote-debugging-port=9223'],
            },
            port: 9010,
            writeToDisk: true
        },
        optimization: {
            minimize: mode === 'production' ? true : false,
            splitChunks: {
                chunks: 'all'
            }
        }
    };
};