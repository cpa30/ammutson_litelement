const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const path = require('path');

const ts_loader = {
    loader: 'ts-loader',
    options: {
        configFile: path.join(__dirname, '/tsconfig.json')
    }
}

const mapEnVars = (env) => {
    const envKeys = Object.keys(env).filter((key) => !key.includes('WEBPACK'));
    return envKeys.map((key) => { return { search: key, replace: env[key] === true ? '' : env[key] }});
}

const string_replace_loader = (env) => {
    return {
        loader: 'string-replace-loader',
        options: {
            multiple: mapEnVars(env)
        }
    }
}
 
module.exports = async (env, mode) => {
    return {
        mode,
        entry: path.join(__dirname, 'src', 'index.ts'),
        output:  {
            path: path.join(__dirname, "/dist"),
            filename: "[name].bundle.js",
            publicPath: '/'
        }, 
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                entry: 'index.ts',
                template: path.resolve(__dirname, 'src/', 'index.html')
            }),
            new CopyWebpackPlugin({
                patterns: [
                    {
                        context: 'node_modules/@webcomponents/webcomponentsjs',
                        from: '**/*.js',
                        to: 'webcomponents'
                    },
                    {
                        context: 'src',
                        from: 'assets',
                        to: 'assets'
                    },
                    {
                        context: 'src',
                        from: 'styles.css',
                        to: 'styles.css'
                    }
                ]
            })
        ],
        module: {
            rules: [
                {
                    test: /API_keys\.ts$/,
                    use: string_replace_loader(env)
                },
                {
                    test: /\.ts$/,
                    use: ts_loader,
                    exclude: /node_modules/
                },
                {
                    test: /\.test\.js$/,
                    use: 'mocha-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.test\.ts$/,
                    use: 'mocha-loader',
                    exclude: /node_modules/
                },
                { 
                    enforce: 'pre',
                    test: /\.js$/,
                    loader: 'source-map-loader'
                }
            ]
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        devtool: mode === 'development' ? 'eval-source-map' : 'cheap-module-source-map',
        watchOptions: {
            ignored: /node_modules/
        },
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
        devServer: {
            contentBase: path.join(__dirname, '/dist'),
            historyApiFallback: true,
            compress: true,
            host: '0.0.0.0',
            port: 9001,
            hot: true //If this is false then liveReload implicitly true
        }
    };
};