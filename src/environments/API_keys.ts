export const API_keys: any = {
    GMapsAPIKey: 'GMAPS_API',
    UntappdAPIKey: 'UNTAPPD_API',
    TwitterAPIKey: 'TWITTER_API'
}