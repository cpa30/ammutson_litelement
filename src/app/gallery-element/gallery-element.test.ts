import  { GalleryElement } from "./gallery-element";
import { assert } from 'chai';

let gallery: GalleryElement;
let shadow: ShadowRoot;

describe("Test Case for the GalleryElement Class", () => {
    beforeEach(() => {
        gallery = new GalleryElement();
        document.body.appendChild(gallery);
    });

    afterEach(() => {
        document.body.removeChild(gallery);
        gallery = null;
    });

    it("gallery has a property 'name' with undefined value", function() {
        assert.strictEqual(gallery.name, undefined);
    });

    it("gallery should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        gallery.updateComplete.then(() => {
            shadow = gallery.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        