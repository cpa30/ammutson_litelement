import { LitElement, html, css, property, customElement } from 'lit-element';
import '../shared/image-collage-element/image-collage-element';

@customElement('gallery-element')
export class GalleryElement extends LitElement {

    @property({ type: String })
    name: string;

    @property()
    images: string[];
    
    constructor() {
        super();
    }

    render() {
        return html`
            <h1 class="title">Gallery</h1>
            <image-collage-element .imageURIs=${this.images}></image-collage-element>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
                background-color: rgba(255,255,255,1);
            }

            .title {
                font-family: Gobold Bold;
                text-align: center;
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
        `;
        return [style];
    }
}
        