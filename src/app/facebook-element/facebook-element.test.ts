import  { FacebookElement } from "./facebook-element";
import { assert } from 'chai';

let facebook: FacebookElement;
let shadow: ShadowRoot;

describe("Test Case for the FacebookElement Class", () => {
    beforeEach(() => {
        facebook = new FacebookElement();
        document.body.appendChild(facebook);
    });

    afterEach(() => {
        document.body.removeChild(facebook);
        facebook = null;
    });

    it("facebook has a property 'name' with value of 'Facebook'", function() {
        assert.strictEqual(facebook.name, 'FacebookElement');
    });

    it("facebook should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        facebook.updateComplete.then(() => {
            shadow = facebook.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        