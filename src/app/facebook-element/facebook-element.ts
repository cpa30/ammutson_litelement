import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('facebook-element')
export class FacebookElement extends LitElement {

    @property({ type: String })
    name: string;
    
    constructor() {
        super();
        this.name = "FacebookElement";
    }

    render() {
        return html`
            <h1>${this.name} Works!</h1>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
        `;
        return [style];
    }
}
        