import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('untappd-embed-element')
export class UntappdEmbedElement extends LitElement {

    @property({ type: String })
    name: string;
    
    constructor() {
        super();
        this.name = "UntappdEmbedElement";
    }

    render() {
        return html`
            ${this.createOldSchoolElement('untappd-container')}
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
        `;
        return [style];
    }


    private loadJSScript(url: string) {
        const script = document.createElement('script');
        script.onload = this.onLoad.bind(this, 'untappd-container');
        script.async = true;
        script.type = 'text/javascript';
        script.src = url;
        return script;
    }

    private createOldSchoolElement(elementID: string) {
        const embeddedScript = this.loadUntappdEmbedScript("21792", "82993");
        document.head.appendChild(embeddedScript);
    }

    private onLoad(elementID: string) {
        const untappdContainer = document.createElement('div');
        untappdContainer.id = elementID;
        window.document.body.appendChild(untappdContainer);
        (<any>window).EmbedMenu.call(window, 'untappd-container');
    }

    private loadUntappdEmbedScript(location: string, theme: string) {
        return this.loadJSScript(`https://business.untappd.com/locations/${location}/themes/${theme}/js`);
    }
}
