import  { UntappdEmbedElement } from "./untappd-embed-element";
import * as assert from 'assert';

let untappdEmbed: UntappdEmbedElement;
let shadow: ShadowRoot;

xdescribe("Test Case for the UntappdEmbedElement Class", () => {
    beforeEach(() => {
        untappdEmbed = new UntappdEmbedElement();
        document.body.appendChild(untappdEmbed);
    });

    afterEach(() => {
        document.body.removeChild(untappdEmbed);
        untappdEmbed = null;
    });

    it("untappdEmbed has a property 'name' with value of 'UntappdEmbed'", function() {
        assert.strictEqual(untappdEmbed.name, 'UntappdEmbedElement');
    });

    it("untappdEmbed should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        untappdEmbed.updateComplete.then(() => {
            shadow = untappdEmbed.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        