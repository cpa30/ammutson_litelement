import  { AboutUsElement } from "./about-us-element";
import {assert} from 'chai';

let aboutUs: AboutUsElement;
let shadow: ShadowRoot;

describe("Test Case for the AboutUsElement Class", () => {
    beforeEach(() => {
        aboutUs = new AboutUsElement();
        document.body.appendChild(aboutUs);
    });

    afterEach(() => {
        document.body.removeChild(aboutUs);
        aboutUs = null;
    });

    it("aboutUs has a property 'name' with value of 'Ammutsøn Craft Beer Family'", function() {
        assert.strictEqual(aboutUs.name, 'Ammutsøn Craft Beer Family');
    });

    it("aboutUs has exactly one 'h1' elements inside its shadow DOM", function(done) {
        aboutUs.updateComplete.then(() => {
            shadow = aboutUs.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
            done();
        });
    });

    it("has one gallery-element as part of it's structure", async function() {
        await aboutUs.updateComplete;
        shadow = aboutUs.shadowRoot;
        const gallerEl = shadow.querySelectorAll('gallery-element');
        assert.strictEqual(gallerEl.length, 1);
    });
})
        