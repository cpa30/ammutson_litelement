import  { HomeElement } from "./home-element";
import { assert } from 'chai';

let home: HomeElement;
let shadow: ShadowRoot;

describe("Test Case for the HomeElement Class", () => {
    beforeEach(() => {
        home = new HomeElement();
        document.body.appendChild(home);
    });

    afterEach(() => {
        document.body.removeChild(home);
        home = null;
    });

    it("home should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        home.updateComplete.then(() => {
            shadow = home.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        