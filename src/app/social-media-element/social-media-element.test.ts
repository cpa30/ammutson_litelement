import  { SocialMediaElement } from "./social-media-element";
import { assert } from 'chai';

let socialMedia: SocialMediaElement;
let shadow: ShadowRoot;

describe("Test Case for the SocialMediaElement Class", () => {
    beforeEach(() => {
        socialMedia = new SocialMediaElement();
        document.body.appendChild(socialMedia);
    });

    afterEach(() => {
        document.body.removeChild(socialMedia);
        socialMedia = null;
    });

    it("socialMedia has a property 'name' with value of 'SocialMedia'", function() {
        assert.strictEqual(socialMedia.name, 'Social Media');
    });

    it("socialMedia should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        socialMedia.updateComplete.then(() => {
            shadow = socialMedia.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        