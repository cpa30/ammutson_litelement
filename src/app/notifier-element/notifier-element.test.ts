import  { NotifierElement } from "./notifier-element";
import { assert, expect } from 'chai';
import { FakeNotifier } from "../../../test/shared/fakes/fakeNotifier.service";
import { Tweet } from "../shared/interfaces/Twitter/Tweet";
import { TweetMetadata } from "../shared/interfaces/Twitter/TweetMetadata";
import { TemplateResult } from "lit-html";

let notifier: NotifierElement;
let shadow: ShadowRoot;

describe("Test Case for the NotifierElement Class", () => {
    beforeEach(() => {
        notifier = new NotifierElement(new FakeNotifier());
        document.body.appendChild(notifier);
    });

    afterEach(() => {
        document.body.removeChild(notifier);
        notifier = null;
    });

    xit("notifier has a property 'name' with value of 'Notifier'", async function() {
        const message = await notifier.getLatestAnnouncement();
        console.log(message);
        assert.isTrue(message instanceof TemplateResult);
    });

    it("notifier should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        notifier.updateComplete.then(() => {
            shadow = notifier.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        