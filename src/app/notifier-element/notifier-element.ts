import { LitElement, html, css, property, customElement } from 'lit-element';
import { Notifier } from '../shared/interfaces/notifier/notifier';
import { Tweet } from '../shared/interfaces/Twitter/Tweet';
import { TweetMetadata } from '../shared/interfaces/Twitter/TweetMetadata';
import { ModalDialogService } from '../shared/services/modal-dialog/modal-dialog.service';

@customElement('notifier-element')
export class NotifierElement extends LitElement {
    private _message: unknown;
    private _notifier: Notifier;

    @property()
    public set notifierClient(value: Notifier) {
        this._notifier = value;
        this.requestUpdate();
    }

    @property() user: string;
    
    constructor(notifier: Notifier) {
        super();
        this._message = "fetching latest announcements...";
        this._notifier = notifier;
    }

    async connectedCallback() {
        super.connectedCallback();
        //await this.getLatestAnnouncement(); //disable temporarily until a proper back-end implementation is in place: see SMTGS service going live
    }

    async getLatestAnnouncement() {
        const messages = (await this._notifier.fetchMessagesForUser(this.user) as {data: Tweet[], meta: TweetMetadata}).data;
        this._message = messages.map((tweet: Tweet) => html`<h3 style="text-align: center">${tweet.text}</h3>`);
        const modalBox = new ModalDialogService();
        modalBox.openModal(html`<h1>${this._message}</h1>`);
        return this._message;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
        `;
        return [style];
    }
}
        