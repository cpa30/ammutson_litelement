import { css, LitElement, html, customElement } from "lit-element";
import '../header-element/header-element';
import '../tap-list-element/tap-list-element';
import '../footer-element/footer-element';
import '../shared/router/nav-outlet-element/nav-outlet-element';
import '../social-media-element/social-media-element';
import '../menu-element/menu-element';
import '../about-us-element/about-us-element';
import '../not-found-element/not-found-element';
import '../home-element/home-element';
import '../notifier-element/notifier-element';
import '../contacts-element/contacts-element';
import { router, Route } from "lit-element-router";
import { TwitterNotifier } from "../shared/services/twitter-notifier/twitter-notifier.service";
import { AjaxService } from "../shared/services/ajax/ajax.service";
import { API_keys } from '../../environments/API_keys';
import { FakeNotifier } from "../../../test/shared/fakes/fakeNotifier.service";

@customElement("app-element")
export class AppElement extends router(LitElement) {
    private route: string;
    private params: Object;
    private query: Object;

    //Abstract somehow away all of the routing so the root component is not heavily loaded.
    static get styles() {
        return css`
            :host {
                display: block;
            }
        `;
    }

    static get routes(): Route[] {
        return [
            {
                name: 'home',
                pattern: ''
            },
            {
                name: 'about_us',
                pattern: 'about_us'
            },
            {
                name: 'tap',
                pattern: 'tap'
            },
            {
                name: 'menu',
                pattern: 'menu'
            },
            {
                name: 'fridge',
                pattern: 'fridge'
            },
            {
                name: 'social_media',
                pattern: 'social_media'
            },
            {
                name: 'contacts',
                pattern: 'contacts'
            },
            {
                name: 'not-found',
                pattern: '*'
            }
        ]
    }

    constructor() {
        super();
        this.route = '';
        this.params = {};
        this.query = {};
    }

    router(route: string, params: Object, query: Object, data: Object) {
        this.route = route;
        this.params = params;
        this.query = query;
    }

    fridgePics: string[] = [
        "assets/cantillon_bottle.jpg",
        "assets/ammutson_fridges.jpg",
        "assets/bfm_with_glasses.jpg"
    ]

    tapPics: string[] = [
        "assets/taps.jpg",
        "assets/taps_with_glasses.jpg",
        "assets/bar_counter.jpg"
    ]

    render() {
        return html`
            <notifier-element user="ammutson" .notifierClient="${new FakeNotifier()}"></notifier-element>
            <header-element class="header"></header-element>
            <div class="body">
                <nav-outlet-element active-route="${this.route}">
                    <menu-element route="menu"></menu-element>
                    <social-media-element route="social_media"></social-media-element>
                    <contacts-element route="contacts"></contacts-element>
                    <tap-list-element route="fridge" name="Fridge List" display="fridge">
                        <gallery-element .images="${this.fridgePics}"></gallery-element>
                    </tap-list-element>
                    <tap-list-element route="tap" name="Tap List" display="tap">
                        <gallery-element .images="${this.tapPics}"></gallery-element>
                    </tap-list-element>
                    <home-element route="home"></home-element>
                    <about-us-element route="about_us"></about-us-element>
                    <not-found-element route="not-found"></not-found-element>
                </nav-outlet-element>
            </div>
        `;
    }
}