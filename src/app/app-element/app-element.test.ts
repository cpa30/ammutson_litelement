/* TODO: implement element replacement (stubbing)
const importMapsModule = document.createElement('script');
importMapsModule.setAttribute('module', 'importmap');
const importMap = {
    imports: {
        TapListElement: '../../../test/shared/stubs/tap-list-element.stub.ts'
    }
}
importMapsModule.textContent = JSON.stringify(importMap);
document.currentScript.before(importMapsModule);
*/

import { AppElement } from './app-element';
import { assert } from 'chai';
import { TapListElement } from '../../../test/shared/stubs/tap-list-element.stub';
//import { TapListElement } from '../tap-list-element/tap-list-element';

let app: AppElement;
let shadow: ShadowRoot;


describe("Test Case for the AppElement Component", function() {
    beforeEach(() => {
        app = new AppElement();
        document.body.appendChild(app);
        app.updateComplete.then(() => {
            const tapListNodes = shadow.querySelectorAll('tap-list-element');
        });
        shadow = app.shadowRoot;
    });

    afterEach(() => {
        document.body.removeChild(app);
        app = null;
    });

    it("contains 3 div containers for the footer, header, content elements", function(done) {
        app.updateComplete.then(() => {
            let divs: any[] = [];
            divs.push(shadow.querySelectorAll('header-element'));
            divs.push(shadow.querySelectorAll('footer-element'));
            divs.push(shadow.querySelectorAll('tap-list-element'));
            assert.strictEqual(divs.length, 3);
            done();
        });
    });

    xit("should have the node for tap-list replaced with a stub", function() {
        const newTap: TapListElement = shadow.querySelector('tap-list-element');
        console.log(newTap);
        console.log(shadow);
        assert.strictEqual('Tap List Stub', newTap.name);
    });
})
        