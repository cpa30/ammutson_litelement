import  { TapListElement } from "./tap-list-element";
import { assert } from 'chai';

let content: TapListElement;
let shadow: ShadowRoot;

describe("Test Case for the TapListElement Component", () => {
    beforeEach(() => {
        content = new TapListElement();
        document.body.appendChild(content);
        shadow = content.shadowRoot;
    });

    afterEach(() => {
        document.body.removeChild(content);
        content = null;
    });

    it("content has a property 'name' with value of 'Tap List'", function() {
        assert.strictEqual(content.name, 'Tap List');
    });
})
        