import { LitElement, css, property, customElement, html } from 'lit-element';
import { UntappdService } from "../shared/services/untappd/untappd.service";
import { AjaxService } from '../shared/services/ajax/ajax.service';
import { Section, Menu } from '../shared/interfaces/Untappd';
import '../list-table-element/list-table-element';
import '../untappd-embed-element/untappd-embed-element';
import { API_keys } from '../../environments/API_keys';

@customElement('tap-list-element')
export class TapListElement extends LitElement {
    private _menu: Menu;
    private _tapList: Section;
    private _bottleList: Section;
    private _linkToUntappd: string = "https://untappd.com/v/ammutson/7481241";

    @property()
    name: string = 'Tap List';

    @property({type: String, attribute: true})
    display: string;

    constructor() {
        super();
        super.connectedCallback();
    }

    connectedCallback() {
        //this.fetchMenu();
    }

    async fetchMenu() {
        // Implement cache mechanism to refresh every 20 min instead of everytime the component is connected.
        const untappd = new UntappdService(
            new AjaxService(), API_keys.UntappdAPIKey);
        this._menu = await untappd.getTapListForLocation(60600, true);
        this._tapList = this._menu.sections.filter(section => section.name === "Tap List")[0];
        this._bottleList = this._menu.sections.filter(section => section.name === "Bottle List")[0];
        this.requestUpdate();
    }

    static get styles() {
        const style = css`
            :host {
                background-color: rgba(0,0,0,1);
            }
            .title {
                font-family: Gobold Bold;
                text-align: center;
            }
            .link-container {
                margin-top: 2%;
                font-family: Gobold Bold;
                text-align: center;
                background-color: rgba(255,255,255,1);
            }
            .link:visited {
                color: rgba(0,0,0,1);
                text-decoration: none;
            }
            .link:hover {
                text-decoration: none;
            }
            .link:active {
                text-decoration: none;
            }
        `;
        return [style];
    }
    
            //<list-table-element pagination="15" .list=${tableContents}></list-table-element>

    public renderTable(tableName: string, tableContents: Section) {
        return html`
            <div class="link-container">
                <a class="link" href="${this._linkToUntappd}">Please follow this link for up-to-date information what's on tap and in the fridge</a>
            </div>
            <slot></slot>
        `;
    }

    private renderRedirectLinks(linkName: string, linkURL: string) {
        return html`
            <a href="${linkURL}">${linkName}</a>
        `;
    }

    render() {
        switch (this.display) {
            case 'tap':
                return this.renderTable(this.name, this._tapList);
            case 'fridge':
                return this.renderTable(this.name, this._bottleList);
            default:
                return html`<span>Nothing here yet...</span>`;
        }
    }
}