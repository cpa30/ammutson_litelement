export class StringUtils {
    static convertSnakeToCamel(stringToConvert: string): string {
        return stringToConvert.replace(/_./g, (fragment: string) => {
            return fragment[1].toUpperCase();
        });
    }

    static removeAllWhitespace(stringWithWhitespace: string) {
        return stringWithWhitespace.replace(/\s+/g,'');
    }
}