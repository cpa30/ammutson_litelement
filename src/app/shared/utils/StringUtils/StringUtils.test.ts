import {assert} from 'chai';
import  { StringUtils } from './StringUtils';

describe("Test Case for the StringUtils Helper Class", () => {

    it("convertSnaketoCamel() should return 'custom_name' as 'customName'", function() {
        const result = StringUtils.convertSnakeToCamel("custom_name");
        assert.strictEqual(result, "customName");
    });

    it("removeAllWhitespace() should return string without whitespaces", () => {
        const result = StringUtils.removeAllWhitespace("     \n\t\t\t\nwhite     space\n");
        assert.strictEqual(result, "whitespace");
    });
});