import { assert } from 'chai';
import { Srlz } from './srlz';
import { Menu } from '../../interfaces/Untappd';
import { menuMock } from '../../../../../test/shared/mocks/untappdMenu';

let srlz: Srlz;

describe("Test Case for the Srlz Class", () => {
    beforeEach(() => {
        srlz = new Srlz();
    });

    afterEach(() => {
        srlz = null;
    });

    it("Serializer is truthy", function() {
        assert.strictEqual(srlz instanceof Srlz, true);
    });
});