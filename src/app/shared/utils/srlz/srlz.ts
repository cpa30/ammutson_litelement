import { StringUtils } from '../StringUtils/StringUtils';

export class Srlz {
    constructor() {}

    toClass<T>(json: any, ctor?: {new(): T}): T {
        const newObject = this.recursivelyConvertSnakeToCamel(json);
        return new ctor();
    }

    transformKeys(obj: any) {
        const newObject = this.recursivelyConvertSnakeToCamel(obj);
        return newObject;
    }

    private recursivelyConvertSnakeToCamel(obj: any): any {
        if (obj === null || obj === undefined) {
            return obj;
        } else if (obj.push) {
            const arr = [];
            for (const arrayItem in obj) {
                arr.push(this.recursivelyConvertSnakeToCamel(obj[arrayItem]));
            }
            return arr;
        } else if (obj instanceof Object) {
            const objProperties = Object.getOwnPropertyNames(obj);
            const newObj: any = {};
            for (const prop in objProperties) {
                const newPrefix = StringUtils.convertSnakeToCamel(objProperties[prop]);
                newObj[newPrefix] = this.recursivelyConvertSnakeToCamel(obj[objProperties[prop]]);
            }
            return newObj;
        } else {
            return obj;
        }
    }
}