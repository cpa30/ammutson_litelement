import  { GmapsElement } from "./gmaps-element";
import { MapType, GestureHandlingOptions, GmapsOptions } from '../interfaces/Gmaps';
import { assert } from 'chai';

let gmaps: GmapsElement;
let shadow: ShadowRoot;

let fakeOpts = {
    GeoCoordinates: {
        Latitude: 48.198535,
        Longitude: 16.35392
    },
    ZoomLevel: 14,
    MapType: MapType.ROADMAP,
    GestureType: GestureHandlingOptions.COOPERATIVE
};

describe("Test Case for the GmapsElement Class", () => {
    beforeEach(() => {
        gmaps = new GmapsElement(fakeOpts, '398guaoesnuth3paoeu39u');
        document.body.appendChild(gmaps);
    });

    afterEach(() => {
        document.body.removeChild(gmaps);
        gmaps = null;
    });

    it("instantiated gmaps is not null", function() {
        assert.notStrictEqual(gmaps, null);
    });

    it("gmaps should have a child Node 'iframe' inside the shadow DOM after it is updated", function(done) {
        gmaps.updateComplete.then(() => {
            shadow = gmaps.shadowRoot; 
            const gmapsIframe: HTMLIFrameElement = shadow.querySelector('iframe');
            assert.notStrictEqual(gmapsIframe, null);
            done();
        });
    });
});
        