import { LitElement, html, css, customElement, property } from 'lit-element';
import { GmapsOptions } from '../interfaces/Gmaps';
import { Dimension } from '../interfaces/Dimension/Dimension';
import { MeasurementUnit } from '../enums/shared/MeasurementUnit.enum';
import { StringUtils } from '../utils/StringUtils/StringUtils';

@customElement('gmaps-element')
export class GmapsElement extends LitElement {
    private _options: GmapsOptions;
    private _apiKey: string;
    private _frameSize: Dimension;
    
    @property()
    public get size(): Dimension {
        return this._frameSize;
    }
    public set size(value: Dimension) {
        const oldVal = this._frameSize;
        this._frameSize = value;
        this.requestUpdate('size', oldVal);
    }

    constructor(options: GmapsOptions, apiKey: string) {
        super();
        this._options = options;
        this._apiKey = apiKey;
        this._frameSize = {
            width: 100,
            height: 100,
            unit: MeasurementUnit.RelativeUnit.PRCT
        }
    }

    render() {
        const formatedSourceLink = StringUtils.removeAllWhitespace(`
            https://www.google.com/maps/embed/v1/place?
            q=${this._options.locationSearchQuery}&
            key=${this._apiKey}&
            center=${this._options.GeoCoordinates.Latitude},${this._options.GeoCoordinates.Longitude}&
            zoom=${this._options.ZoomLevel}&
            maptype=${this._options.MapType}
        `);

        return html`
            <span class="title">Google Maps©</span>
            <iframe
                width="${this._frameSize.height}${this._frameSize.unit}"
                height="${this._frameSize.width}${this._frameSize.unit}"
                frameborder="0"
                style="border:0"
                src="${formatedSourceLink}">
            </iframe>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                height: cover;
                width: 100%;
            }
        `;
        return [style];
    }
}
        