export enum GestureHandlingOptions {
    COOPERATIVE = 'cooperative',
    GREEDY = 'greedy',
    NONE = 'none',
    AUTO = 'auto'
}