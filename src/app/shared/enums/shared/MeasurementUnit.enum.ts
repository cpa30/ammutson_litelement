enum AbsoluteUnit {
    PX = 'px',
    PT = 'pt',
    PC = 'pc',
    IN = 'in',
    CM = 'cm',
    MM = 'mm'
}

enum RelativeUnit {
    EM = 'em',
    EX = 'ex',
    CH = 'ch',
    REM = 'rem',
    VW = 'vw',
    VH = 'vh',
    VMIN = 'vmin',
    VMAX = 'vmax',
    PRCT = '%'
}

export const MeasurementUnit = { RelativeUnit, AbsoluteUnit };

export type MeasurementUnit = RelativeUnit | AbsoluteUnit;