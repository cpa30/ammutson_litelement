export enum TeaType {
    GREEN = "green",
    BLACK = "black",
    FRUIT = "fruit",
    HERBAL = "herbal",
    OTHER = "other"
}