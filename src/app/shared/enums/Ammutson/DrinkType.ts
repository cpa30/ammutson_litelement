export enum DrinkType {
    WINE = "wine",
    TEA = "tea",
    COFFEE = "coffee",
    SPIRIT = "spirit",
    ALCOHOLFREE = "alcoholfree",
    OTHER = "other",
    SOFTDRINK = "softdrink",
    JUICE = "juice"
}