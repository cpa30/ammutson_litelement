export enum WineType {
    RED = "red",
    WHITE = "white",
    ROSE = "rose"
}