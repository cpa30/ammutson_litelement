export enum SpiritType {
    SCHNAPS = "schnaps",
    RUM = "rum",
    WHISKEY = "whiskey",
    GIN = "gin",
    TEQUILLA = 'tequilla',
}