export enum ConsumableType {
    FOOD = "food",
    WINE = "wine",
    BEER = "beer",
    SPIRITS = "spirits",
    NONALCOHOLIC = "nonalcoholic",
    COFFEEANDTEA = "coffee&tea",
}