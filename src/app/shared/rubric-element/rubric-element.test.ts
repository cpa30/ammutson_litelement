import  { RubricElement } from "./rubric-element";
import { assert } from 'chai';

let rubric: RubricElement;
let shadow: ShadowRoot;

describe("Test Case for the RubricElement Class", () => {
    beforeEach(() => {
        rubric = new RubricElement();
        document.body.appendChild(rubric);
    });

    afterEach(() => {
        document.body.removeChild(rubric);
        rubric = null;
    });

    it("rubric has a property 'name' with no default value", function() {
        assert.strictEqual(rubric.name, undefined);
    });

    it("rubric should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        rubric.updateComplete.then(() => {
            shadow = rubric.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        