import { LitElement, html, css, property, customElement } from 'lit-element';
import { MenuItem } from '../interfaces/Ammutson/MenuItem';

@customElement('rubric-element')
export class RubricElement extends LitElement {

    @property({ type: String })
    name: string;

    @property()
    items: MenuItem[];
    
    constructor() {
        super();
    }

    render() {
        return html`
            <span class="title">${this.name}</span>
            ${this.renderMenuTable()}
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: flex;
                flex-direction: column;
                justify-content: space-between;
            }

            .title {
                background-color: black;
                color: white;
                font-family: "Gobold Regular";
                text-align: center;

            }
            .menu-table {
                display: none;
            }
            .subheading {
                font-size: 1.5rem;
                font-weight: bold;
            }
            .menu-item-volume {
                text-align: center;
            }
            .menu-item-price {
                text-align: center;
            }
            .menu-item-detail {
                border: 1px solid;
            }
        `;
        return [style];
    }

    renderMenuTable() {
        return html`
            <table class="menu-table" name="${this.name}">
                <tr>
                    <th>Name</th>
                    <th>Vol</th>
                    <th>Price</th>
                </tr>
            ${this.items?.map((item: MenuItem) => item.subtype)
                .filter(this.distinct)
                .map((item: string) => {
                    return html`
                        <tr class="subheading">${item}</tr>
                        ${this.items?.filter((menuitem: MenuItem) => menuitem.subtype === item)
                            .map((menuitem: MenuItem, ix: number) => {
                                return html`
                                    <tr>
                                        <td @click=${this.toggleMenuItemVisibility.bind(this, menuitem, ix)}>${menuitem.name}</td>
                                        <td class="menu-item-volume">${menuitem.volume}</td>
                                        <td class="menu-item-price">${menuitem.price}</td>
                                    </tr>
                                    <tr style="display: none" id="menu-${this.toKebapCase(menuitem.subtype)}-detail-${ix}">
                                        <td class="menu-item-detail" >${menuitem.detail}</td>
                                    </tr>
                                `;
                        })}
                    `;
                })}
            </table>
        `;
    }

    // move to separate utils class
    private distinct(value: any, index: any, self: any) {
        return self.indexOf(value) === index;
    }
    
    private toggleMenuItemVisibility(item: MenuItem, index: number) {
        const tableItem = this.shadowRoot.querySelector<HTMLTableElement>(`#menu-${this.toKebapCase(item.subtype)}-detail-${index}`);
        tableItem.style.display = tableItem.style.display === 'none' ? 'table-row' : 'none';
    }

    private toKebapCase(value: string) {
        return value.replace(' ', '-').toLowerCase();
    }
}
