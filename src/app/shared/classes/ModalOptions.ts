import { Dimension } from '../interfaces/Dimension/Dimension';
import { MeasurementUnit } from '../enums/shared/MeasurementUnit.enum';

export class ModalOptions implements Dimension {
    width: number;
    height: number;
    unit: MeasurementUnit;

    constructor() {
        this.width = 100;
        this.height = 100;
        this.unit = MeasurementUnit.AbsoluteUnit.PX;
    }
}