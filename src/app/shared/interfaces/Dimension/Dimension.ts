import { MeasurementUnit } from "../../enums/shared/MeasurementUnit.enum";

export interface Dimension {
    width: number;
    height: number;
    unit: MeasurementUnit;
}