import { BeverageDescription } from '../Untappd';

export interface Beverage {
    abv: string;
    brewery: string;
    breweryLocation: string;
    calories: string;
    cask: boolean;
    containers: string[];
    createdAt: Date;
    custom: BeverageDescription;
    description: string;
    ibu: string;
    id: number;
    inProduction: boolean;
    labelImage: URL;
    labelImageHd: URL;
    name: string;
    nitro: boolean;
    original: BeverageDescription;
    position: number;
    rating: string;
    sectionId: number;
    style: string;
    tapNumber: number;
    untappdBeerSlug: string;
    untappdBreweryId: number;
    untappdId: number;
    updatedAt: Date;
}