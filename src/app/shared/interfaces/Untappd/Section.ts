import { BeverageDescription } from '../Untappd';
import { SectionType } from '../../enums/Untappd';

export interface Section {
    createdAt: Date;
    description: string;
    id: number;
    locationId: number;
    items: BeverageDescription[];
    menuId: number;
    name: string;
    position: number;
    public: boolean;
    type: SectionType;
    pushNotificationFrequency: string;
    sections: Section[];
    showPriceOnUntappd: boolean;
    unpublished: boolean;
    updatedAt: Date;
}