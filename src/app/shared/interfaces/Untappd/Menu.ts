import { Section } from '../Untappd';

export interface Menu {
    createdAt: Date;
    description: string;
    draft: boolean;
    footer: string;
    id: number;
    locationId: number;
    name: string;
    onDeckSection: Section;
    position: number;
    pushNotificationFrequency: string;
    sections: Section[];
    showPriceOnUntappd: boolean;
    unpublished: boolean;
    updatedAt: Date;
    // Change to Guid
    uuid: string;
}