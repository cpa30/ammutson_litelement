export interface BeverageDescription {
    abv: string;
    brewery: string;
    calories: string;
    description: string;
    ibu: string;
    name: string;
    style: string;
    rating: string;
    labelImage: string;
    labelImageHd: string;
    tapNumber: number;
}