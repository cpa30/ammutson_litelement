export interface TweetMetadata {
    oldestID: string;
    newestID: string;
    resultCount: number;
    nextToken: string;
}