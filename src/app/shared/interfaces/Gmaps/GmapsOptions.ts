import { MapType } from "../../enums/Gmaps/MapType.enum";
import { GestureHandlingOptions } from "../../enums/Gmaps/GestureHandlingOptions.enum";

export interface GmapsOptions {
    GeoCoordinates: Coordinates;
    ZoomLevel: number;
    MapType: MapType;
    GestureType: GestureHandlingOptions;
    locationSearchQuery?: string
}

interface Coordinates {
    Latitude: number;
    Longitude: number;
    Altitude?: number;
}