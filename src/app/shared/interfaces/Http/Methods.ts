import { Options } from "../Http";

export interface Methods {
    get(url: string, options?: Options): Promise<Response>;
    post(url: string, json: string, options?: Options): Promise<Response>;
    put(url: string, json: string, options?: Options): Promise<Response>;
    delete(url: string, options?: Options): Promise<Response>;
    headers: Headers;
}