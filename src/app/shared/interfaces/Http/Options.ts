export interface Options {
    Headers?: Headers;
    HttpParams?: URLSearchParams;
}