export interface Notifier {
    fetchMessage(uRI: string): Promise<unknown>;
    fetchMessagesForUser(userID: string): Promise<unknown>;
}