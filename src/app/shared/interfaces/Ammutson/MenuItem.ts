import { ConsumableType } from '../../enums/Ammutson/ConsumableType';
import { FoodType } from '../../enums/Ammutson/FoodType';
import { SpiritType } from '../../enums/Ammutson/SpiritType';
import { TeaType } from '../../enums/Ammutson/TeaType';
import { WineType } from '../../enums/Ammutson/WineType';

export interface MenuItem {
    name: string;
    type: ConsumableType;
    subtype: SpiritType | WineType | TeaType | FoodType;
    volume: string;
    price: number;
    detail: string;
}