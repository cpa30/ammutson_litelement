import { DrinkType } from "../../enums/Ammutson/DrinkType";
import { MenuItem } from "./MenuItem";

export interface Drink {
    brand: string;
    name: string;
    type: DrinkType;
    volume: number;
    price: number;
}