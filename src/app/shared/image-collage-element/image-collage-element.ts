import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('image-collage-element')
export class ImageCollageElement extends LitElement {

    @property({ type: String })
    name: string;
    
    @property()
    imageURIs: string[] = ["assets/Ammutson_Stamp_black.svg"];

    constructor() {
        super();
    }

    render() {
        return html`
            ${this.imageURIs?.map(uri => html`<img src="${uri}" />`)}
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: flex;
                justify-content: space-around;
                align-items: center;
                padding: 1%;
            }

            img {
                height: 25%;
                width: 25%;
            }
        `;
        return [style];
    }
}
        