import  { ImageCollageElement } from "./image-collage-element";
import { assert } from 'chai';

let imageCollage: ImageCollageElement;
let shadow: ShadowRoot;

describe("Test Case for the ImageCollageElement Class", () => {
    beforeEach(() => {
        imageCollage = new ImageCollageElement();
        document.body.appendChild(imageCollage);
    });

    afterEach(() => {
        document.body.removeChild(imageCollage);
        imageCollage = null;
    });

    it("imageCollage should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        imageCollage.updateComplete.then(() => {
            shadow = imageCollage.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });

    it("contains an array property imageURIs with initialized value 'assets/Ammutson_Stamp_black.svg'", async function() {
        await imageCollage.updateComplete;
        assert.strictEqual("assets/Ammutson_Stamp_black.svg", imageCollage.imageURIs[0]);
    });
});
        