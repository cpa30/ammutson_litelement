import { InstagramRedirectElement } from "../../../instagram-redirect-element/instagram-redirect-element";
import { MeasurementUnit } from "../../enums/shared/MeasurementUnit.enum";
import { AjaxService } from "../ajax/ajax.service";
import { ModalDialogService } from '../modal-dialog/modal-dialog.service';

export class InstagramService {
    private _instagramAccessToken: string;
    private MAX_POST_COUNT: number = 20;
    private _userID: string = '622621235076049';
    private _http: AjaxService;
    private _redirectUri: string = `https://smtgs.ammutson.com/auth/instagram`;
    private _modal: ModalDialogService = new ModalDialogService({width: 100, height: 100, unit: MeasurementUnit.AbsoluteUnit.PX});

    constructor(http: AjaxService, userID: string) {
        this._http = http;
        this._userID = userID;
        this._instagramAccessToken = localStorage.getItem('instagram_access_token');
    }


    // number of Posts should be deferred to the number of posts per page?! Incorporate somehow with pagination.
    async getCurrentUserStory(numberOfPosts: number = this.MAX_POST_COUNT) {
        if (this._instagramAccessToken !== null) {
            return await this._http.get(
                `https://graph.instagram.com/${this._userID}?`+
                `fields=id,username&`+
                `access_token=${this._instagramAccessToken}`);
        } else {
            const authCode = await this.getUserAuthorizationCode();
            console.log(authCode);
            //localStorage.setItem('instagram_access_token', authCode);
        }
    }

    async getUserAuthorizationCode() {
        const instagramPromptRedirect = new InstagramRedirectElement(this._userID, this._redirectUri);
        this._modal.openModal(instagramPromptRedirect);
    }
}