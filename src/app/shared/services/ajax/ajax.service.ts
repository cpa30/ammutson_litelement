import { Options, Methods} from '../../interfaces/Http';

export class AjaxService implements Methods {
    private _headers: Headers;

    constructor(headers?: Headers) {
        this._headers = headers;
    }

    public get headers() {
        return this._headers;
    }

    public set headers(value: Headers) {
        this._headers = value;
    }
    async get(url: string, options?: Options): Promise<Response> {
        const request = new Request(url, {method: 'GET', headers: options?.Headers ?? this._headers});
        return fetch(request);
    }

    async post<T>(url: string, payload: string | FormData, options?: Options): Promise<Response> {
        const request = new Request(url, {method: 'POST', headers: options?.Headers ?? this._headers, body: payload})
        return fetch(request);
    }

    async put(url: string, json: string, options?: Options): Promise<Response> {
        const request = new Request(url, {method: 'PUT', body: json, headers: options?.Headers ?? this._headers})
        return fetch(request);
    }

    async delete(url: string, options?: Options): Promise<Response> {
        const request = new Request(url, {method: 'DELETE', headers: options?.Headers ?? this._headers});
        return fetch(request);
    }
}