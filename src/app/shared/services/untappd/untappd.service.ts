import { Methods } from '../../interfaces/Http';
import { Menu } from '../../interfaces/Untappd';
import { Srlz } from '../../utils/srlz/srlz';

export class UntappdService {
    private authToken: string;
    private httpAgent: Methods;
    
    set headers(value: Headers) {
        this.httpAgent.headers = value;
    }

    constructor(ajax: Methods, authorizationToken: string) {
        this.authToken = authorizationToken;
        this.httpAgent = ajax;
        if (this.httpAgent?.headers?.has("Authorization") === undefined) {
            const headers = new Headers();
            headers.append("Authorization", `Basic ${this.authToken}`)
            this.httpAgent.headers = headers;
        }
    }

    getTapListForLocation(locationId: number, fullDescription: boolean = true): Promise<Menu> {
        const locationStrg = locationId.toString();
        const description = fullDescription.toString();
        const serializer = new Srlz();
        return this.httpAgent
            .get(`https://business.untappd.com/api/v1/menus/${locationStrg}?full=${description}`)
            .then((response: any) => {
                return response.json();
            }).then((json: any) => {
                const newJson = serializer.transformKeys(json);
                return newJson.menu;
            });
    }
}