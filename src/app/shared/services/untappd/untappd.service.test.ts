import  { UntappdService } from "../untappd/untappd.service";
import { FakeAjaxService } from "../../../../../test/shared/fakes/fakeAjax.service";
import { assert } from 'chai';

let untappd: UntappdService;

describe("Test Case for the UntappdService Class", () => {
    beforeEach(() => {
        untappd = new UntappdService(new FakeAjaxService(), "fake token");
    });

    afterEach(() => {
        untappd = null;
    });

    it("should return a Promise of class Menu when calling getTapListForLocation()", function() {
        const results = untappd.getTapListForLocation(69420, true);
        assert.strictEqual(results instanceof Promise, true);
    });
})
        