import { Methods } from "../../interfaces/Http";
import { Notifier } from "../../interfaces/notifier/notifier";

export class TwitterNotifier implements Notifier {
    _httpAgent: Methods;

    constructor(httpAgent: Methods) {
        this._httpAgent = httpAgent;
    }

    fetchMessage(uRI: string): Promise<unknown> {
        throw new Error("Method not implemented.");
    }

    fetchMessagesForUser(userID: string): Promise<unknown> {
        return this._httpAgent.get(`https://api.twitter.com/2/users/${userID}/tweets`);
    }
}