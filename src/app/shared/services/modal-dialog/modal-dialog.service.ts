import { Dimension } from '../../interfaces/Dimension/Dimension';
import { ModalDialogElement } from '../../modal-dialog-element/modal-dialog-element';
import { TemplateResult } from 'lit-element';
import { MeasurementUnit } from '../../enums/shared/MeasurementUnit.enum';

export class ModalDialogService {
    #_options: Dimension;

    constructor(options: Dimension = {width: 100, height: 100, unit: MeasurementUnit.RelativeUnit.PRCT}) {
        this.#_options = options;
    }

    get options() {
        return this.#_options;
    }

    openModal(templateString: string|Node|TemplateResult) {
        const newModal = new ModalDialogElement(templateString, this.#_options);
        newModal.attachModal();
    }
}