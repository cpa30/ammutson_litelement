import  { NavOutletElement } from "./nav-outlet-element";
import { assert, expect } from 'chai';

let navOutlet: NavOutletElement;
let shadow: ShadowRoot;

describe("Test Case for the NavoutletElement Class", () => {
    beforeEach(() => {
        navOutlet = new NavOutletElement();
        document.body.appendChild(navOutlet);
    });

    afterEach(() => {
        document.body.removeChild(navOutlet);
        navOutlet = null;
    });

    it("navOutlet is truthy", function() {
        navOutlet.updateComplete.then(() => {
            expect(navOutlet).to.equal(true);
        });
    });

    it("navoutlet should have a child Node 'slot' inside the shadow DOM after it is updated", function(done) {
        navOutlet.updateComplete.then(() => {
            shadow = navOutlet.shadowRoot; 
            const slot = shadow.querySelectorAll('slot');
            assert.strictEqual(slot.length, 1);
            done();
        });
    });
});
        