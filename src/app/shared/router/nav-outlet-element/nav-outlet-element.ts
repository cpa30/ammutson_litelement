import { LitElement, html, customElement } from 'lit-element';
import { outlet } from 'lit-element-router';

@customElement('nav-outlet-element')
export class NavOutletElement extends outlet(LitElement) {

    constructor() {
        super();
    }

    render() {
        return html`
            <slot></slot>
        `;
    }
}
        