import { LitElement, html, css, customElement, property } from 'lit-element';
import { navigator } from 'lit-element-router';

@customElement('navlink-element')
export class NavLinkElement extends navigator(LitElement) {

    @property({type: String})
    href: string

    constructor() {
        super();
        this.href = '';
    }

    render() {
        return html`
            <a href="${this.href}" @click="${this.linkClick}">
                <slot></slot>
            </a>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block-inline;
            }

            a {
                color: rgba(0,0,0,1);
            }

            a:link {
                text-decoration: none;
            }
        `;
        return [style];
    }

    linkClick(e: Event) {
        e.preventDefault();
        const routerLinkClicked = new CustomEvent('router-link-clicked', {
            detail: this.href,
            composed: true,
            bubbles: true
        });
        this.dispatchEvent(routerLinkClicked);
        this.navigate(this.href);
    }
}
        