import  { NavLinkElement } from "./nav-link-element";
import { assert, expect } from 'chai';

let navlink: NavLinkElement;
let shadow: ShadowRoot;

describe("Test Case for the NavlinkElement Class", function() {
    beforeEach(() => {
        navlink = new NavLinkElement();
        document.body.appendChild(navlink);
    });

    afterEach(() => {
        document.body.removeChild(navlink);
        navlink = null;
    });

    it("navlink element is truthy", function() {
        navlink.updateComplete.then(() => {
            expect(navlink).to.equal(true);
        });
    });
});
        