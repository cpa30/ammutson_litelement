import { LitElement, html, css, property, customElement, TemplateResult } from 'lit-element';
import { ModalOptions } from '../classes/ModalOptions';

@customElement('modal-dialog-element')
export class ModalDialogElement extends LitElement {
    private _options: ModalOptions;
    private _template: string|TemplateResult|Node;
    
    constructor(template: string|TemplateResult|Node, options: ModalOptions) {
        super();
        this.containerTemplate = template;
        this._options = options;
    }

    @property()
    get containerTemplate(): string|TemplateResult|Node {
        return this._template;
    };
    set containerTemplate(value) {
        const oldVal = this._template;
        this._template = value;
        this.requestUpdate('containerTemplate', oldVal);
    }

    render() {
        return html`
            <button class="dialog-close-btn" @click="${this.removeModal}">X</button>
            <div class="template-container">
                ${this._template}
            </div>
        `;
    }

    public attachModal() {
        document.body.appendChild(this);
    }

    private removeModal() {
        document.body.removeChild(this);
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
                position: fixed;
                z-index: 100;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: auto;
                background-color: rgb(0,0,0);
                background-color: rgb(0,0,0,0.7);
            }
            .dialog-close-btn {
                position: absolute;
                top: 2%;
                left: 1%;
                width: 2rem;
                height: 2rem;
                background-color: rgba(255,255,255,0.3);
                border: 0;
                border-radius: 100%;
                cursor: pointer;
            }
            .dialog-close-btn:hover {
                background-color: rgba(255,255,255,1);
            }

            .template-container {
                top: 25%;
                position: relative;
                margin: 0 5%;
                height: 50%;
                color: rgb(255,255,255);
            }
        `;
        return [style];
    }
}
        