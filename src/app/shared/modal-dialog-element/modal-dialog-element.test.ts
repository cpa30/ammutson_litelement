import  { ModalDialogElement } from "./modal-dialog-element";
import { assert } from 'chai';
import { MeasurementUnit } from "../enums/shared/MeasurementUnit.enum";
import { html } from "lit-element";

let modalDialog: ModalDialogElement;
let shadow: ShadowRoot;

describe("Test Case for the ModalDialogElement Class", () => {

    it("modalDialog is truthy", function() {
        const template = html`<span>Stub</span>`
        modalDialog = new ModalDialogElement(template, {width: 50, height: 50, unit: MeasurementUnit.AbsoluteUnit.PX});
        assert.strictEqual(modalDialog instanceof ModalDialogElement, true);
    });

    it("modalDialog supplied with a Stub string within a 'span' tag has the string as the innerHTML property", (done) => {
        const template = html`<span>Stub</span>`
        modalDialog = new ModalDialogElement(template, {width: 50, height: 50, unit: MeasurementUnit.AbsoluteUnit.PX});
        modalDialog.attachModal();
        modalDialog.updateComplete.then(() => {
            shadow = modalDialog.shadowRoot;
            const span: HTMLSpanElement = shadow.querySelector('span');
            const closeButton: HTMLButtonElement = shadow.querySelector('button.dialog-close-btn');
            assert.strictEqual(span.innerHTML, 'Stub');
            closeButton.click();
            done();
        });
    });
});
        