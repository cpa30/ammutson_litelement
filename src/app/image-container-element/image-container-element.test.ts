import  { ImageContainerElement } from "./image-container-element";
import { Section } from '../shared/interfaces/Untappd';
import { assert } from 'chai';

let imageContainer: ImageContainerElement;
let containerShadow: ShadowRoot;
let untappd: Section;

describe("Test Case for the ImageContainerElement Class", () => {
    beforeEach(() => {
        imageContainer = new ImageContainerElement();
        document.body.appendChild(imageContainer);
    });

    afterEach(() => {
        document.body.removeChild(imageContainer);
        imageContainer = null;
    });

    it("imageContainer has a property 'name' with value of 'ImageContainer'", function() {
        assert.strictEqual(imageContainer.name, 'ImageContainerElement');
    });

    it('should have an <img> html element', (done) => {
        imageContainer.updateComplete.then(() => {
            containerShadow = imageContainer.shadowRoot;
            const img = containerShadow.querySelectorAll('img');
            assert.strictEqual(img.length, 1);
            done();
        });
    });
})
        