import { LitElement, html, css, property, customElement } from 'lit-element';
import { AjaxService } from '../shared/services/ajax/ajax.service';

// This Element(component) should be in the shared folder!
@customElement('image-container-element')
export class ImageContainerElement extends LitElement {

    private ajax: AjaxService;

    @property() public name: string;
    @property() public src: string;
    
    constructor() {
        super();
        this.name = "ImageContainerElement";
        this.ajax = new AjaxService();
    }

    render() {
        return html`
            <img src="${this.src}" />
        `;
    }

    handleFallback(ev: ErrorEvent) {
        this.src = "assets/Ammutson_Stamp_black.svg";
    }

    private prefetchSource() {
        this.ajax.get(this.src).catch(this.handleFallback);
    }
    
    static get styles() {
        const style = css`
            img {
                width: 3em;
                height: 3em;
            }
            img:before {
                content: ' ';
                display: block;
                position: absolute;
                width: 3em;
                height: 3em;
                background-image: url(assets/Ammutson_Stamp_black.svg);
            }
        `;
        return [style];
    }
}
        