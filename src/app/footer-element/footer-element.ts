import { LitElement, html, css, property, customElement } from 'lit-element';
import { ModalDialogService } from '../shared/services/modal-dialog/modal-dialog.service';
import { MeasurementUnit } from '../shared/enums/shared/MeasurementUnit.enum';
import { GmapsElement } from '../shared/gmaps-element/gmaps-element';
import { GmapsOptions, MapType, GestureHandlingOptions } from '../shared/interfaces/Gmaps';
import { API_keys } from '../../environments/API_keys';

@customElement('footer-element')
export class FooterElement extends LitElement {
    private modalService: ModalDialogService;
    
    @property()
    name: string;

    constructor() {
        super();
        this.modalService = new ModalDialogService({width: 100, height: 100, unit: MeasurementUnit.RelativeUnit.PRCT});
    }

    render() {
        return html`
            <footer>
                <div class="footer how-to-find-us">${this.howToFindUs()}</div>
                <div class="footer opening-hours">${this.openingHours()}</div>
                <div class="footer contact-info">${this.contactInfo()}</div>
                <div class="footer attribution-clause">${this.attributionLicenses()}</div>
            </footer>
        `;
    }
    
    static get styles() {
        const style = css`
            footer {
                display: flex;
                width: 100%;
                flex-direction: column;
                justify-content: space-between;
            }
            .opening-hours {
                display: inline-block;
            }
            .how-to-find-us {
                display: inline-block;
            }
            .contact-info {
                display: inline-block;
            }
            span {
                display: block;
            }
            .map-directions:hover {
                cursor: pointer;
                background-color: black;
                color: white;
            }
            .title {
                font-family: Gobold Bold;
            }
            .attribution-clause {
                font-size: 10px;
                padding-top: 2%;
            }
        `;
        return [style];
    }

    private openingHours() {
        return html`
            <h4 class="title">Opening Hours*</h4>
            <span>Monday - Thursday</span>
            <span>1600 - 0200</span>
            <span>Friday</span>
            <span>1600 - 0400</span>
            <span>Saturday</span>
            <span>1400 - 0400</span>
            <span>Sunday</span>
            <span>1400 - 0000</span>
            <h4>*Regular Hours</h4>
        `;
    }

    private howToFindUs() {
        return html`
            <h4 class="title">How To Find Us</h4>
            <h5>*Click the Address to Get Directions on Map</h5>
            <address class="map-directions" @click="${this.openDirectionsMap}">Barnabitengasse 10, 1060 Vienna, Austria</address>
        `;
    }

    private contactInfo() {
        return html`
            <h4 class="title">Contact Information and Reservations</h4>
            <span class="contact-details owner">Misho Omar</span>
            <span class="contact-details">+43 (664) 4799130</span>
        `;
    }

    private openDirectionsMap() {
        const mapOptions: GmapsOptions = {
            GeoCoordinates: {
                Latitude: 48.198535,
                Longitude: 16.35392
            },
            ZoomLevel: 14,
            MapType: MapType.ROADMAP,
            GestureType: GestureHandlingOptions.COOPERATIVE,
            locationSearchQuery: 'Ammutson Craft Beer Dive, Vienna, Austria'
        };

        const gmaps = new GmapsElement(mapOptions, API_keys.GMapsAPIKey);
        this.modalService.openModal(gmaps);
    }

    private attributionLicenses() {
        return html`
            <span>
                Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a>
                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
            </span>
            <span>
                Textures by Artyom Saqib
                <a href="https://artsqb.com">https://artsqb.com</a>
            </span>
            <span>
                Page Design by <a href="https://www.instagram.com/tamtamsari/">Sarah Häckel</a> and <a href="https://twitter.com/cpa45">Cristian Pogolsha</a>
            </span>
        `;
    }
}
        