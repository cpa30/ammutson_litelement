import  { FooterElement } from "./footer-element";
import {assert} from 'chai';

let footer: FooterElement;
let shadow: ShadowRoot;

describe("Test Case for the FooterElement Component", () => {
    beforeEach(() => {
        footer = new FooterElement();
        document.body.appendChild(footer);
        shadow = footer.shadowRoot;
    });

    afterEach(() => {
        document.body.removeChild(footer);
        footer = null;
    });

    it("should contain four div containers for 'contact info', 'opening hours' and 'how to find us', 'attribution license for icons'", async() => {
        await footer.updateComplete;
        const divs = footer.shadowRoot.querySelectorAll('.footer');
        assert.strictEqual(divs.length, 4);
    });
})
        