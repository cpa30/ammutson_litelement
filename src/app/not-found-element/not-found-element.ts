import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('not-found-element')
export class NotFoundElement extends LitElement {

    @property({ type: String })
    name: string;
    
    constructor() {
        super();
        this.name = "404 Not Found";
    }

    render() {
        return html`
            <h1>${this.name} &#x1F6AB;</h1>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
        `;
        return [style];
    }
}
        