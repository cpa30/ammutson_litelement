import  { NotFoundElement } from "./not-found-element";
import { assert } from 'chai';

let notFound: NotFoundElement;
let shadow: ShadowRoot;

describe("Test Case for the NotFoundElement Class", () => {
    beforeEach(() => {
        notFound = new NotFoundElement();
        document.body.appendChild(notFound);
    });

    afterEach(() => {
        document.body.removeChild(notFound);
        notFound = null;
    });

    it("notFound has a property 'name' with value of 'NotFound'", function() {
        assert.strictEqual(notFound.name, '404 Not Found');
    });

    it("notFound should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        notFound.updateComplete.then(() => {
            shadow = notFound.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        