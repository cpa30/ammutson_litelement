import  { HeaderElement } from "./header-element";
import { assert, expect } from 'chai';

let header: HeaderElement;
let shadow: ShadowRoot;

describe("Test Case for the HeaderElement Component", () => {
    beforeEach(() => {
        header = new HeaderElement();
        document.body.appendChild(header);
        shadow = header.shadowRoot;
    });

    afterEach(() => {
        document.body.removeChild(header);
        header = null;
    });

    it("header element is truthy", function() {
        header.updateComplete.then(() => {
            expect(header).to.equal(true);
        });
    });

    it("should contain seven navlink-element(s) for each navigation link", (done) => {
        header.updateComplete.then(() => {
            const divs = header.shadowRoot.querySelectorAll('navlink-element');
            assert.strictEqual(divs.length, 7);
            done();
        });
    });
})
        