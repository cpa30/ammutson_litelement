import  { ListTableElement } from "./list-table-element";
import { assert } from 'chai';
import { menuMock } from '../../../test/shared/mocks/untappdMenu';

let listTable: ListTableElement;

describe("Test Case for the ListTableElement Class", () => {
    beforeEach(() => {
        listTable = new ListTableElement();
        listTable.list = menuMock.sections[0];
        document.body.appendChild(listTable);
    });

    afterEach(() => {
        document.body.removeChild(listTable);
        listTable = null;
    });

    it("listTable is truthy", () => {
        assert.notStrictEqual(listTable, undefined);
    });
})
        