import { LitElement, html, css, property, customElement } from 'lit-element';
import '../image-container-element/image-container-element';
import { BeverageDescription, Section } from '../shared/interfaces/Untappd';

@customElement('list-table-element')
export class ListTableElement extends LitElement {
    private _beverages: BeverageDescription[];
    private _listName: string;
    private _list: Section;
    // TODO: find a way to programmatically set-up the number of columns in the table:
    // based on the number of suplied th elements. -> The problem with this is an endless loop.
    private _columnsInTable: number = 7;

    @property()
    public pagination: number = 10;

    @property({type: Object})
    get list(): Section {
        return this._list;
    };

    set list(value) {
        const oldValue = this._list;
        this._list = value;
        this.connectedCallback();
        this.requestUpdate('list', oldValue);
    }

    private numberOfPages: number;

    constructor() {
        super();
        super.connectedCallback();
    }

    connectedCallback() {
        if (this.list !== undefined) {
            this._listName = this.list.name;
            this._beverages = this.list.items;
        }
    }

    static get styles() {
        return css`
            :host {
                display: block;
                background-color: rgba(255,255,255,1);
            }
            table {
                border: black solid 2px;
                width: 100%;
            }
            .title {
                text-align: center;
                font-family: Gobold Bold;
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
            .highlight-entry {
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
            .data-placeholder {
                text-align: center;
                font-weight: bold;
            }
            tr:hover {
                cursor: pointer;
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
            .button {
                box-shadow: none;
                display: inline-block;
                color: rgba(255,255,255,1);
                background-color: rgba(0,0,0,1);
                width: 2.2em;
                margin: 0.5em;
                border: 0em;
            }
            .pagination {
                display: block;
            }
        `;
    }

    render() {
        if (this.list === undefined) {
            return this.renderLoadingPlaceholder();
        } else if (this._beverages?.length === 0) {
            return this.renderEmptyListPlaceholder();
        } else {
            return this.renderTable();
        }
    }

    private paginateTable() {
            this.numberOfPages = Math.floor((this._beverages.length / this.pagination));
            const remainder = this._beverages.length % this.pagination;
            if (remainder !== 0)
                this.numberOfPages += 1;
            let pagesMarkUp = new Array(this.numberOfPages).fill('');
            return pagesMarkUp
                .map((item: unknown, ix: number) => html`<button class="button" @click="${this.changePage.bind(this, ix+1)}">${ix+1}</button>`);
    }

    private changePage(page: number) {
        const otherHtmlTableRows = [...this.shadowRoot.querySelectorAll<HTMLTableRowElement>(`tr[class^="table-page-"]`)];
        otherHtmlTableRows.map((htmltr: HTMLTableRowElement, ix: number) => {
            if (htmltr.classList.contains(`table-page-${page}`)) {
                htmltr.style.setProperty('display', 'table-row');
                this.hideDetailedInfo(htmltr);
            } else {
                htmltr.style.setProperty('display', 'none')
            }
            return htmltr;
        });
    }

    hideDetailedInfo(tr: HTMLTableRowElement) {
        if (tr.id.includes('info')) {
            tr.style.setProperty('display', 'none');
        } else {
            tr.classList.remove('highlight-entry');
        }
    }

    private renderTable() {
        return html`
            <h1 class="title">${this._listName}</h1>
            <h3 class="comment">*For additional information, click on each entry</h3>
            <h3 class="comment">
                Content Presentation is Powered by <a target="_blank" href="https://untappd.com/v/ammutson/7481241">Untappd.</a>
            </h3>
            ${this.paginateTable()}
            <table name="${this._listName}">
                <tr>
                    <th></th>
                    <th></th>
                    <th>Style</th>
                    <th>Name</th>
                    <th>Brewery</th>
                    <th>Cal</th>
                    <th>ABV</th>
                    <th>Rating</th>
                </tr>
                ${this._beverages?.map((beverage: BeverageDescription, ix: number) => {
                    return html`
                        <tr class="${`table-page-${(Math.floor(ix / this.pagination))+1}`}"
                            style="display: ${(Math.floor(ix / this.pagination))+1 === 1 ? "table-row" : "none"}"
                            @click="${this.toggleShow.bind(this, beverage.tapNumber ?? ix+1, null)}"
                            id="${this._listName}-entry-${beverage.tapNumber ?? ix+1}"
                        >
                            <td>${beverage.tapNumber ?? ix+1}</td>
                            <td><image-container-element src="${beverage.labelImage}"></image-container-element></td>
                            <td>${beverage.style}</td>
                            <td>${beverage.name}</td>
                            <td>${beverage.brewery}</td>
                            <td>${beverage.calories}</td>
                            <td>${beverage.abv}</td>
                            <td>${beverage.rating}</td>
                        </tr>
                        <tr 
                            class="${`table-page-${(Math.floor(ix / this.pagination))+1}`}"
                            style="display: none;"
                            id="${this._listName}-info-${beverage.tapNumber ?? ix+1}"
                        >
                            <td colspan="${this._columnsInTable}">
                                ${beverage.description === "" ? this.descriptionPlaceholder() : beverage.description}
                            </td>
                        </tr>
                    `;
                })}
            </table>
        `;
    }

    private renderLoadingPlaceholder() {
        return html`<span class="data-placeholder">Waiting for data...</span>`;
    }

    private renderEmptyListPlaceholder() {
        return html`<span class="data-placeholder">No Information Available. List is empty.</span>`;
    }

    private toggleShow(index: number, visible?: boolean) {
        const selectedElement = this.shadowRoot.querySelector<HTMLTableRowElement>(`[id="${this._listName}-entry-${index}"]`);
        const selectedElementInfo = this.shadowRoot.querySelector<HTMLTableRowElement>(`[id="${this._listName}-info-${index}"]`);
        const visibility = visible ?? getComputedStyle(selectedElementInfo).display === "none" ? true : false;
        if (visibility === false) {
            selectedElementInfo.style.setProperty('display', 'none');
            selectedElement.classList.remove('highlight-entry');
        } else {
            selectedElementInfo.style.setProperty('display', 'table-row');
            selectedElement.classList.add('highlight-entry');
        }
    }

    private descriptionPlaceholder() {
        return html`No Description Available &#x26D4;`;
    }
}