import  { InstagramRedirectElement } from "./instagram-redirect-element";
import { assert, expect } from 'chai';

let instagramRedirect: InstagramRedirectElement;
let shadow: ShadowRoot;

describe("Test Case for the InstagramredirectElement Class", () => {
    beforeEach(() => {
        const fakeAppId = 'aoesutahoeu';
        const fakeRedirect = 'aosear.aeosnuthau';
        instagramRedirect = new InstagramRedirectElement(fakeAppId, fakeRedirect);
        instagramRedirect.querySelectorAll('iframe').forEach((node) => {node.replaceWith()});
        document.body.appendChild(instagramRedirect);
    });

    afterEach(() => {
        document.body.removeChild(instagramRedirect);
        instagramRedirect = null;
    });

    it("instagramredirect has a property 'name' with value of 'Instagramredirect'", function() {
        assert.strictEqual(instagramRedirect.name, 'Instagram Redirect');
    });


    it("instagramredirect should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        instagramRedirect.updateComplete.then(() => {
            shadow = instagramRedirect.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        