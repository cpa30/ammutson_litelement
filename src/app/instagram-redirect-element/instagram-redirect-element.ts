import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('instagram-redirect-element')
export class InstagramRedirectElement extends LitElement {

    private _appId: string;
    private _redirectUri: string;

    @property({ type: String })
    name: string;
    
    constructor(appId: string, redirectUri: string) {
        super();
        this.name = "Instagram Redirect";
        this._appId = appId;
        this._redirectUri = redirectUri
    }

    render() {
        return html`
            <iframe id="redirectIFrame" src="${this.authorizationRequestUri}"></iframe>
        `;
    }

    public get authorizationRequestUri() {
        const code: string = `https://api.instagram.com/oauth/authorize`+
            `?client_id=${this._appId}`+
            `&redirect_uri=${this._redirectUri}`+
            `&scope=user_profile,user_media`+
            `&response_type=code`;
        return code;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
                background-color: white;
            }

            iframe {
                width: 100%;
                height: 100%;
                border: 0;
            }
        `;
        return [style];
    }
}
        