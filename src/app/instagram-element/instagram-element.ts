import { LitElement, html, css, property, customElement } from 'lit-element';

@customElement('instagram-element')
export class InstagramElement extends LitElement {

    @property({ type: String })
    name: string;
    
    constructor() {
        super();
        this.name = "InstagramElement";
    }

    render() {
        return html`
            <h1>${this.name} Works!</h1>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
        `;
        return [style];
    }
}
        