import  { InstagramElement } from "./instagram-element";
import { assert } from 'chai';

let instagram: InstagramElement;
let shadow: ShadowRoot;

describe("Test Case for the InstagramElement Class", () => {
    beforeEach(() => {
        instagram = new InstagramElement();
        document.body.appendChild(instagram);
    });

    afterEach(() => {
        document.body.removeChild(instagram);
        instagram = null;
    });

    it("instagram has a property 'name' with value of 'Instagram'", function() {
        assert.strictEqual(instagram.name, 'InstagramElement');
    });

    it("instagram should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        instagram.updateComplete.then(() => {
            shadow = instagram.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        