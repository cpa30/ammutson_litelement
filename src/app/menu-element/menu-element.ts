import { LitElement, html, css, property, customElement } from 'lit-element';
import { MenuItem } from '../shared/interfaces/Ammutson/MenuItem';
import { ConsumableType } from '../shared/enums/Ammutson/ConsumableType';
import { AjaxService } from '../shared/services/ajax/ajax.service';
import '../shared/rubric-element/rubric-element';
import '../gallery-element/gallery-element';

@customElement('menu-element')
export class MenuElement extends LitElement {
    private http: AjaxService;

    @property({ type: String })
    name: string;

    @property()
    list: MenuItem[];
    
    constructor() {
        super();
        this.name = "Menu";
        this.http = new AjaxService();
        super.connectedCallback();
    }

    async connectedCallback() {
        const response = await (await this.http.get("assets/menu_items.json")).json();
        this.list = response.menuItems;
    }

    imagesInMenu: string[] = [
        "assets/board_games.jpg",
        "assets/cheese_platter.jpg",
        "assets/wall_art.jpg"
    ];

    render() {
        return html`
            <div class="menu-wrapper">
                <h1 class="title">${this.name}</h1>
                ${this.renderByType(ConsumableType.WINE)}
                ${this.renderByType(ConsumableType.SPIRITS)}
                ${this.renderByType(ConsumableType.NONALCOHOLIC)}
                ${this.renderByType(ConsumableType.COFFEEANDTEA)}
                ${this.renderByType(ConsumableType.FOOD)}
            </div>
                ${this.renderAlergensLegend()}
                ${this.renderInfo()}
            <gallery-element class="gallery" .images=${this.imagesInMenu}></gallery-element>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
            .menu-wrapper {
                background-color: rgba(255,255,255,1);
            }
            .gallery {
                border-top: 5%;
            }
            .title {
                font-family: Gobold Bold;
                text-align: center;
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
            .subtitle {
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
                font-size: 1.0rem;
                font-weight: normal;
                width: absolute;
            }
            rubric-element {
                cursor: pointer;
            }
            .drop-down-effect {
                animation-duration: 1s;
                animation-name: drop-down;
            }
            @keyframes drop-down {
                from {
                    margin-top: 0%;
                }
                to {
                    margin-top: 100%;
                }
            }
            .rise-up-effect {
                animation-duration: 1s;
                animation-name: rise-up;
            }
            @keyframes rise-up {
                from {
                    margin-top: 100%;
                }
                to {
                    margin-top: 0%;
                }
            }
            .section-wrapper {
                background-color: rgba(255,255,255,1);
                margin-top: 2em;
            }
        `;
        return [style];
    }

    renderInfo() {
        return html`
            <div class="section-wrapper">
                <div class="subtitle">Info</div>
                Click on the menu items to see more details.
            </div>
        `;
    }
    renderAlergensLegend() {
            return html`
                <div class="section-wrapper">
                    <div class="subtitle">Alergens</div>
                    <table>
                        <tr id="A">
                            <td>A</td>
                            <td>can contain gluten</td>
                        </tr>
                        <tr id="C">
                            <td>C</td>
                            <td>can contain egg</td>
                        </tr>
                        <tr id="G">
                            <td>G</td>
                            <td>can contain milk or lactose</td>
                        </tr>
                    </table>
                </div>
            `;
    }

    renderByType(drinkType: ConsumableType) {
        const listByType = this.list?.filter(item => item.type === drinkType);
        return html`
            <rubric-element class="rubric"
                            @click="${this.hideUnhide.bind(this, drinkType.toString())}"
                            name="${drinkType.toString()}"
                            .items=${listByType}>
            </rubric-element>
        `;
    }

    hideUnhide(name: string) {
        // TODO: move to shared functionality.
        const shadows = [...this.shadowRoot.querySelectorAll(`rubric-element`)];
        shadows.map((element) => {
            const table: HTMLTableElement = element.shadowRoot.querySelector('table');
            const tableName = table.getAttribute('name');
            if (tableName === name) {
                table.style.setProperty('display', 'table');
                table.classList.add('drop-down-effect')
            } else {
                table.style.setProperty('display', 'none');
                table.classList.add('rise-up-effect');
            }
        });
    }
}
        