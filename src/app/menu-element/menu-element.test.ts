import  { MenuElement } from "./menu-element";
import { assert } from 'chai';

let menu: MenuElement;
let shadow: ShadowRoot;

describe("Test Case for the MenuElement Class", () => {
    beforeEach(() => {
        menu = new MenuElement();
        document.body.appendChild(menu);
    });

    afterEach(() => {
        document.body.removeChild(menu);
        menu = null;
    });

    it("menu has a property 'name' with value of 'Menu'", function() {
        assert.strictEqual(menu.name, 'Menu');
    });

    it("menu should have a child Node 'h1' inside the shadow DOM after it is updated", function() {
        menu.updateComplete.then(() => {
            shadow = menu.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
        });
    });
});
        