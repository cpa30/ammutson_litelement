import { LitElement, html, css, property, customElement } from 'lit-element';
import '../footer-element/footer-element';
import '../gallery-element/gallery-element';

@customElement('contacts-element')
export class ContactsElement extends LitElement {

    @property({ type: String })
    name: string;
    
    constructor() {
        super();
        this.name = "Contact Us";
    }

    imagesToRender: string[] = [
        "assets/team_july2020.jpg",
        "assets/team_dec2019.jpg",
        "assets/team_october2020.jpg"
    ];

    render() {
        return html`
            <div class="contact-us-wrapper">
                <h1>${this.name}</h1>
                <footer-element></footer-element>
            </div>
            <gallery-element name="Team" .images="${this.imagesToRender}"></gallery-element>
        `;
    }
    
    static get styles() {
        const style = css`
            :host {
                display: block;
            }
            .contact-us-wrapper {
                background-color: rgba(255,255,255,1);
            }
            h1 {
                text-align: center;
                background-color: rgba(0,0,0,1);
                color: rgba(255,255,255,1);
            }
        `;
        return [style];
    }
}
        