import  { ContactsElement } from "./contacts-element";
import { assert } from 'chai';

let contacts: ContactsElement;
let shadow: ShadowRoot;

describe("Test Case for the ContactsElement Class", () => {
    beforeEach(() => {
        contacts = new ContactsElement();
        document.body.appendChild(contacts);
    });

    afterEach(() => {
        document.body.removeChild(contacts);
        contacts = null;
    });

    it("contacts has a property 'name' with value of 'Contact Us'", function() {
        assert.strictEqual(contacts.name, 'Contact Us');
    });

    it("contacts should have a child Node 'h1' inside the shadow DOM after it is updated", (done) => {
        contacts.updateComplete.then(() => {
            shadow = contacts.shadowRoot; 
            const h1 = shadow.querySelectorAll('h1');
            assert.strictEqual(h1.length, 1);
            done();
        });
    });
});
        